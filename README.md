# Described Link

The Described Link module extends the core link field to add a description text
field, with a default widget, formatter, and template.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/described_link).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/described_link).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Navigate to Administration > Extend and enable the module. No configuration is
   necessary. There is now a Decribed Link field that provides a description text
   field, with a default widget.


## Maintainers

- Andy Hebrank - [ahebrank](https://www.drupal.org/u/ahebrank)

**Supporting organization:**

- [NewCity](https://www.drupal.org/newcity)
